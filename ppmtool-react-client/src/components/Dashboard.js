import React, { Component } from 'react'
import ProjectItem from './Project/ProjectItem';
import CreateProjectButton from './CreateProjectButton';

const arrayTeste = ["ola", "comovai", "tudobem"];
class Dashboard extends Component {


    render() {
        return (
            <React.Fragment>
                <div className="projects">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <h1 className="display-4 text-center">Projects</h1>
                                <br />
                                <CreateProjectButton />
                                <br />
                                <hr />
                                {arrayTeste.map(item => <ProjectItem texto={item} />)}
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
export default Dashboard