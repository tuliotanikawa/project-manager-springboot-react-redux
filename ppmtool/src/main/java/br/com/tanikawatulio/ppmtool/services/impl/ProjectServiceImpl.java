package br.com.tanikawatulio.ppmtool.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.tanikawatulio.ppmtool.domain.Project;
import br.com.tanikawatulio.ppmtool.exceptions.ProjectIdException;
import br.com.tanikawatulio.ppmtool.repositories.ProjectRepository;
import br.com.tanikawatulio.ppmtool.services.ProjectService;

@Service
public class ProjectServiceImpl implements ProjectService {

	@Autowired
	private ProjectRepository projectRepository;

	@Override
	public Project saveOrUpdateProject(Project project) {
			try {
				project.setProjectIdentifier(project.getProjectIdentifier().toUpperCase());
				return projectRepository.save(project);
			} catch (Exception e) {
				throw new ProjectIdException("Project ID '" + project.getProjectIdentifier().toUpperCase()
						+ "' already exists on the database.");
			}
	}

	@Override
	public Project findProjectByIdentifier(String projectId) {
		Project project = projectRepository.findByProjectIdentifier(projectId.toUpperCase());
		if (project == null)
			throw new ProjectIdException("No Project found with this identifier '" + projectId.toUpperCase() + "'");
		return project;
	}

	@Override
	// Iterable returns out of the box a JSON
	public Iterable<Project> findAllProject() {
		return projectRepository.findAll();
	}

	@Override
	public void deleteProjectByIdentifier(String projectId) {
		Project project = projectRepository.findByProjectIdentifier(projectId.toUpperCase());
		if (project == null)
			throw new ProjectIdException(
					"Cannot delete '" + projectId.toUpperCase() + "' because it doesn't exists on the database");
		projectRepository.delete(project);
	}

}
