package br.com.tanikawatulio.ppmtool.services;

import br.com.tanikawatulio.ppmtool.domain.Project;

public interface ProjectService {

	public Project saveOrUpdateProject(Project project);

	public Project findProjectByIdentifier(String projectId);

	public Iterable<Project> findAllProject();
	
	public void deleteProjectByIdentifier(String projectId);
	
}
